import puppeteer from 'puppeteer';
import express from 'express';

async function init_browser() {
  const browser = await puppeteer.launch({
    headless: true,
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
    ]
  });
  const page = await browser.newPage();

  await page.setViewport({
    width: 1920,
    height: 100000,
    deviceScaleFactor: 1,
  });

  await page.goto('https://5etools.iridium-server.com/5etools.html', { waitUntil: 'networkidle0' });

  await page.click('#navbar > li:nth-child(8)');
  await page.click('#navbar > li.dropdown.dropdown--navbar.page__nav-hidden-mobile.page__btn-nav-root.open > li > li:nth-child(1) > a');

  async function get_from_selectors(url, selectors, extractor) {
    async function get_list(url, selector) {
      await page.goto(url, { waitUntil: 'networkidle0' });
      return page.$eval(selector, (list) => {
        return Object.values(list.children)
          .map(child => child.innerText)
          .map(text => { console.log(text); return text; })
          .map(text => text.split('\n'));
      });
    }

    const res = [];
    for (const selector of selectors) {
      const list = (await get_list(url, selector)).map(extractor);
      res.push(list);
    }
    return res.flat();
  }

  function select_from_first_and_last(url, selectors) {
    const first_and_last = text => {
      const name = text[0];
      const source = text.slice(-1)[0];
      return { name, source };
    };

    return get_from_selectors(url, selectors, first_and_last);
  }

  function select_from_second_and_last(url, selectors) {
    const second_and_last = text => {
      const name = text[1];
      const source = text.slice(-1)[0];
      return { name, source };
    };

    return get_from_selectors(url, selectors, second_and_last);
  }

  const spells = await select_from_first_and_last('https://5etools.iridium-server.com/spells.html', ['#listcontainer > ul']);
  const items = await select_from_first_and_last('https://5etools.iridium-server.com/items.html', ['#listcontainer > div.flex-col.min-h-0.w-100.itm__wrp-list.itm__wrp-list--mundane > ul', '#listcontainer > div.flex-col.min-h-0.w-100.itm__wrp-list.itm__wrp-list--magic > ul']);
  const conditions = await select_from_second_and_last('https://5etools.iridium-server.com/conditionsdiseases.html', ['#listcontainer > ul']);
  const monsters = await select_from_first_and_last('https://5etools.iridium-server.com/bestiary.html', ['#listcontainer > ul']);
  const actions = await select_from_first_and_last('https://5etools.iridium-server.com/actions.html', ['#listcontainer > ul']);
  return { page, spells, items, conditions, monsters, actions };
}

function get_text_builder(page) {
  const path = '/home/jbassin/projects/javascript/dnd-compendium-service/element.png';

  return async (url) => {
    await page.setViewport({
      width: 463,
      height: 100000,
      deviceScaleFactor: 1,
    });

    await page.goto(url, { waitUntil: 'networkidle0' });

    const clip = await page.evaluate(selector => {
      const element = document.querySelector(selector); // eslint-disable-line no-undef
      const { x, y, width, height } = element.getBoundingClientRect();
      return { x, y, width, height };
    }, '#pagecontent');

    await page.screenshot({ path, clip });
    return path;
  };
}

async function make_endpoint(app, source, name, url) {
  app.get(`/${name}`, async (_, res) => {
    const names = source.map(({ name }) => name);
    res.jsonp(names);
  });

  app.post(`/${name}`, async ({ body: { name } }, res) => {
    try {
      const { name: unsanitized_name, source: unsanitized_source } = source.find(el => el.name.toLowerCase() === name.toLowerCase());
      const sanitized_name = encodeURIComponent(unsanitized_name.toLowerCase());
      const sanitized_source = unsanitized_source.toLowerCase();
      const fetch_url = `${url}#${sanitized_name}_${sanitized_source}`;

      const path = await get_text(fetch_url);
      res.sendFile(path);
    } catch (error) {
      res.status(500).jsonp(error);
    }
  });
}

const { page, spells, items, conditions, monsters, actions } = await init_browser();
const get_text = get_text_builder(page);

const app = express();
const port = 5000;

app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));

make_endpoint(app, spells, 'spell', 'https://5etools.iridium-server.com/spells.html');
make_endpoint(app, items, 'item', 'https://5etools.iridium-server.com/items.html');
make_endpoint(app, conditions, 'condition', 'https://5etools.iridium-server.com/conditionsdiseases.html');
make_endpoint(app, monsters, 'monster', 'https://5etools.iridium-server.com/bestiary.html');
make_endpoint(app, actions, 'action', 'https://5etools.iridium-server.com/actions.html');

app.listen(port, () => {
  console.log(`Compendium Service active at http://localhost:${port}`);
});